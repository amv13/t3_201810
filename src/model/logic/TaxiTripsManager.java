package model.logic;

import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager 
{

	// TODO
	// Definition of data model 
	
	//----------------------------
	//Atributos
	//----------------------------
	
	/** Organizaci�n FirstIn FirstOut */
	private Queue<Service> enCola;
	
	/** Organizaci�n LastIn FirstOut*/
	private Stack<Service> enPila;
	
	//------------------
	//M�todos
	//------------------
	
	/**
	 * Carga sevicios 
	 */
	public void loadServices(String serviceFile, String taxiId) 
	{
		
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		
		//Carga en cola
		enCola = new Queue<Service>();
				
		//Carga en pila
		enPila = new Stack<Service>();
		
		JsonParser parser = new JsonParser();
		
		try 
		{
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-medium.json";
			
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size();i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				/* Mostrar un JsonObject (Servicio taxi) */
				
				if( obj.get("taxi_id").getAsString().equals(taxiId))
				{
					
					/* Obtener la propiedad trip_id de un servicio (String) */
					String triId ="NaN";
					if(obj.get("trip_id") != null)
					{
						triId = obj.get("trip_id").getAsString();
					}
					
					/* Obtener la propiedad trip_seconds de un servicio (int) */
					int trScnds = -1; 
					if(obj.get("trip_seconds") != null)
					{
						trScnds = obj.get("trip_seconds").getAsInt();
					}
					
					/* Obtener la propiedad trip_miles de un servicio (double) */
					double miles = -1; 
					if(obj.get("trip_miles") != null)
					{
						miles = obj.get("trip_miles").getAsDouble();;
					}
						
					/* Obtener la propiedad trip_total de un servicio (double) */
					double total = -1; 
					if(obj.get("trip_total") != null)
					{
						total = obj.get("trip_total").getAsDouble();
					}
					
					/* Obtener la propiedad trip_start_timestamp de un servicio (String) */
					String startTime = "NaN";
					if(obj.get("trip_start_timestamp") != null)
					{
						startTime = obj.get("trip_start_timestamp").getAsString();
					}
					
					Service service = new Service(triId, taxiId, trScnds, miles, total, startTime);
				
					enCola.enqueue(service);
					enPila.push(service);
				}
			}
		}
		catch (Exception e3){
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}	
		
	}

	/**
	 * 
	 */
	@Override
	public int [] servicesInInverseOrder() {
		
		//Usa pila
		System.out.println("Inside servicesInInverseOrder");
		
		Service first = enPila.pop();
		Service next = enPila.pop();
		
		int cumple = 1;
		int noCumple = 0;
		
		while(first != null && next != null)
		{
			if(first.compararPorFecha(next)==-2)
				break;
			if(first.compararPorFecha(next)==-1)
			{
				noCumple++;
				next = enPila.pop();
			}
			//Si empiezan al mismo tiempo tambi�n esta ordenado
			else
			{
				cumple++;
				first = next;
				next = enPila.pop();
			}
		}
		
		int [] resultado = new int[2];
		resultado[0] = noCumple;
		resultado[1] = cumple;
		return resultado;
	}

	@Override
	public int [] servicesInOrder() {
		
		//Usa cola
		System.out.println("Inside servicesInInverseOrder");
		
		Service first = enCola.dequeue();
		Service next = enCola.dequeue();
		
		int cumple = 1;
		int noCumple = 0;
		
		while(first != null && next != null)
		{
			if(first.compararPorFecha(next) == -2)
				break;
			if(first.compararPorFecha(next)==-1)
			{
				noCumple++;
				next = enCola.dequeue();
			}
			//Si empiezan al mismo tiempo tambi�n esta ordenado
			else
			{
				cumple++;
				first = next;
				next = enCola.dequeue();
			}
		}
		
		int [] resultado = new int[2];
		resultado[0] = cumple;
		resultado[1] = noCumple;
		return resultado;
	}


}
