package model.vo;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	//-----------------------------
	//Atributos
	//------------------------------
	
	private String tripId;

	private String taxiId;

	private int tripSeconds;

	private double tripMiles;

	private double tripTotal;

	private String tripStartTimeStamp;

	

	//Constructor
	public Service(String triId, String txId, int trScnds, double miles, double total, String startTime)
	{
		tripId = triId;
		taxiId = txId;
		tripSeconds = trScnds;
		tripMiles = miles;
		tripTotal = total;
		tripStartTimeStamp = startTime;
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTimeStamp() {
		// TODO Auto-generated method stub
		return tripStartTimeStamp;
	}

	/**
	 * Compara los servicios seg�n el tripId
	 * @param o Servicio a comparar con this
	 * @return 0 si los tripId son iguales
	 *         1 si los tripId son distintos
	 */
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		if(tripId.equals(o.tripId)){
			return 0;
		}
		return 1;

	}
	
	/**
	 * Compara los servicios seg�n el tripStartTime
	 * @param o Servicio a comparar con this
	 * @return 0 si this empez� primero o al mismo tiempo que el servicio por par�metro
	 * 		   -1 si this empez� despu�s que el servicio por par�metro
	 * 		   -2 Si la fecha est� en un formato incorrecto
	 */
	public int compararPorFecha(Service o) {
		// TODO Auto-generated method stub
		Date tst;Date otroTst;
		try {
			tst = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(this.tripStartTimeStamp);
			otroTst = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(o.tripStartTimeStamp);
			if(tst.after(otroTst))
				return -1;
			else 
				return 0;

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -2;
		}

	}
}
